//
//  StateManagementApp.swift
//  StateManagement
//
//  Created by Joao Fonseca on 06/09/2021.
//

import SwiftUI

@main
struct StateManagementApp: App {
    var body: some Scene {
        WindowGroup {
            StateManagerMainView(state: AppState())
        }
    }
}

//
//  CounterView.swift
//  CounterView
//
//  Created by Joao Fonseca on 06/09/2021.
//

import SwiftUI
import Combine

struct PrimeId : Identifiable {
    var id: Int {val}
    let val : Int
}

final class AppState: ObservableObject, Identifiable {
    var count = 0 {
        didSet { self.objectWillChange.send() }
    }
    
    var favoritesPrimes: [PrimeId] = [] {
        didSet { self.objectWillChange.send() }
    }
    
    let objectWillChange = PassthroughSubject<Void, Never>()
}

struct NthPrime : Identifiable {
    var id: Int { nthPrime }
    let nthPrime : Int
}

struct CounterView: View {
    @ObservedObject var state : AppState
    @State private var isPrimeModalShown = false
    @State private var alertPrime : NthPrime?
    
    var body: some View {
        VStack{
            HStack{
                Button("-") {
                    self.state.count -= 1
                }
                Text("\(self.state.count)")
                Button("+") {
                    self.state.count += 1
                }
            }
            Button("Is this prime?") {
                self.isPrimeModalShown = true
            }
            Button("What is the \(ordinal(self.state.count)) prime?") {
                nthPrime(self.state.count) { prime in
                    guard let prime = prime else {
                        return
                    }
                    
                    self.alertPrime = NthPrime(nthPrime: prime)
                }
            }
        }
        .font(.title)
        .navigationTitle("Counter Demo")
        .sheet(isPresented: $isPrimeModalShown, onDismiss: {
            self.isPrimeModalShown = false
        },content: {
            PrimeView(state: self.state)
        })
        .alert(item: $alertPrime, content: { n in
            Alert(title: Text("The \(ordinal(self.state.count)) prime is \(n.nthPrime)"), dismissButton: .default(Text("Ok")))
        })
    }
}

private func ordinal(_ n:Int)->String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .ordinal
    return formatter.string(for: n) ?? ""
}

struct CounterView_Previews: PreviewProvider {
    static var previews: some View {
        CounterView(state: AppState())
    }
}

//
//  PrimeView.swift
//  PrimeView
//
//  Created by Joao Fonseca on 08/09/2021.
//

import SwiftUI

struct PrimeView: View {
    @ObservedObject var state : AppState
    
    var body: some View {
        VStack{
            if isPrime(self.state.count){
                Text("\(self.state.count) is prime :D")
                if self.state.favoritesPrimes.contains(where: {$0.val == self.state.count}) {
                    Button(action: {
                        self.state.favoritesPrimes.removeAll(where: {$0.val == self.state.count})
                    }) {
                        Text("Remove from favorites")
                    }
                } else {
                    Button(action: {
                        self.state.favoritesPrimes.append(PrimeId(val: self.state.count))
                    }) {
                        Text("Save to favorites")
                    }
                }
            }else{
                Text("\(self.state.count) is not prime :(")
            }
        }
    }
}

private func isPrime(_ p:Int) -> Bool{
    if p <= 1 { return false }
    if p <= 3 { return true }
    for i in 2...Int(sqrtf(Float(p))) {
        if p % i == 0 { return false }
    }
    return true
}

struct PrimeView_Previews: PreviewProvider {
    static var previews: some View {
        PrimeView(state: AppState())
    }
}

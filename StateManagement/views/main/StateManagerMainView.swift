//
//  StateManagerMainView.swift
//  StateManagement
//
//  Created by Joao Fonseca on 06/09/2021.
//

import SwiftUI

struct StateManagerMainView: View {
    @ObservedObject var state : AppState
    
    var body: some View {
        NavigationView{
            List{
                NavigationLink(destination: CounterView(state: self.state)) {
                    Text("Counter demo")
                }
                NavigationLink(destination: FavoritesView(state:self.state)) {
                    Text("Favorite primes")
                }
            }
            .navigationTitle("State Management")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        StateManagerMainView(state: AppState())
    }
}

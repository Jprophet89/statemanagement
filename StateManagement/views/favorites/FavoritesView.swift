//
//  FavoritesView.swift
//  FavoritesView
//
//  Created by Joao Fonseca on 08/09/2021.
//

import SwiftUI

struct FavoritesView: View {
    @ObservedObject var state : AppState
    var body: some View {
        List{
            ForEach(self.state.favoritesPrimes) { prime in
                Text("\(prime.val)")
            }.onDelete { IndexSet in
                for index in IndexSet{
                    self.state.favoritesPrimes.remove(at: index)
                }
            }
        }.navigationTitle(Text("Favorite Primes"))
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView(state:AppState())
    }
}

//
//  WolframAlphaResult.swift
//  WolframAlphaResult
//
//  Created by Joao Fonseca on 08/09/2021.
//

import SwiftUI

public let wolframAlphaApiKey = "KAUAA3-2KP4AKTK6W"

struct WolframeAlphaResult : Decodable{
    let queryresult : QueryResult
    
    struct QueryResult : Decodable{
        let pods: [Pod]
        
        struct Pod: Decodable {
            let primary : Bool?
            let subpods : [SubPod]
            
            struct SubPod : Decodable{
                let plaintext : String
            }
        }
    }
}

func wolframAlpha(query: String, callback: @escaping (WolframeAlphaResult?) -> Void) {
    guard var components = URLComponents(string: "https://www.wolframalpha.com/api/v2/query") else {
        callback(nil)
        return
    }
    
    components.queryItems = [
        URLQueryItem(name: "appid", value: wolframAlphaApiKey),
        URLQueryItem(name: "input", value: query),
        URLQueryItem(name: "format", value: "plaintext"),
        URLQueryItem(name: "output", value: "JSON"),
    ]
    
    if let url = components.url(relativeTo: nil) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                callback(nil)
                return
            }
            
            do {
                let result = try JSONDecoder().decode(WolframeAlphaResult.self, from: data)
                callback(result)
            }catch{
                callback(nil)
            }
            
        }
        .resume()
    }else{
        callback(nil)
    }
}

func nthPrime(_ n:Int, callBack:@escaping (Int?) -> Void){
    wolframAlpha(query: "prime \(n)") { result in
        guard let textNumber = result?.queryresult.pods.first(where: { $0.primary == .some(true) })?.subpods.first?.plaintext, let prime = Int(textNumber) else {
            callBack(nil)
            return
        }
        
        callBack(prime)
    }
}
